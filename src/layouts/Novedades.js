import React, {Component} from 'react';
import {View,Text, ScrollView, FlatList, TouchableOpacity} from 'react-native';
import {BtnAction}  from '../components/BtnAction';
import {IconText} from "../components/IconText";
import {Actions} from 'react-native-router-flux';
import axios from 'axios';
import {connect} from 'react-redux';
const route = 'http://api.cronosseguridad.com/';

class Novedades extends Component{

    constructor(props) {
        super(props);
        this.props.setHeader('Novedades');
        this.state = {
            novedades: [],
            nextButton: (this.props.nextButton === undefined)?true:false
        };
    }

    async componentDidMount(){
        try{
            let novedades = await this.requestNovedadesHoy(this.props.usuario.usuario.data.id_edificio);
            if(novedades.data.status == 200){
                if(novedades.data.message == 'No hay novedades hoy.'){
                    this.setState({novedades: [{fecha: '-',descripcion: novedades.data.message,id_personal: '-'}]});
                }
                else{
                    this.setState({novedades: novedades.data.result});
                }
            }
            else{
                this.setState({novedades: [{fecha: '-',descripcion: 'Sin novedades',id_personal: '-'}]});
                throw "Error en la app";
            }
        }
        catch(err){
            console.log(err);
        }

    }

    async requestNovedadesHoy(idEdificio) {
        console.log(this.props.usuario);
        return axios({
            method: 'GET',
            url: route+'novedades/hoy/'+idEdificio,
            headers: {
                "Auth-Key": "cronos-ak-v1",
                "X-Cronos-Auth-Token": this.props.usuario.token,
                }
          });
    }

    renderNovedad(novedad){
        return(
            <TouchableOpacity onPress={() => Actions.imageView({txt: novedad.descripcion , img: novedad.imagen_url})} style={styles.rowContainer}>
                <Text style={[{fontSize: 16,color: 'black',alignSelf: 'flex-start',textAlign: 'center'}, styles.otherDataRow]}>{novedad.fecha}</Text>
                <Text style={[styles.rowText, styles.mainDataRow,{alignItems: 'flex-start'}]}>{novedad.descripcion}</Text>
                <Text style={[styles.rowText, styles.otherDataRow]}>{novedad.id_personal}</Text>
            </TouchableOpacity>
        );
    }

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.row}>
                    <IconText txt='Presione la novedad para ver foto'/>
                </View>

                <View style={styles.row}>
                    <ScrollView style={[styles.scrollView]}>
                        <View>
                            <View style={styles.subContainer}>
                                <Text style={[styles.titleText, styles.otherData]}>Fecha/Hora</Text>
                                <Text style={[styles.titleText, styles.mainData]}>Descripción</Text>
                                <Text style={[styles.titleText, styles.otherData]}>Vigilante</Text>
                            </View>
                        </View>
                        {/* <View>/ */}
                        <FlatList style={{marginBottom: 5, height: 200}}
                            data={this.state.novedades}
                            extraData={this.state}
                            keyExtractor={(item, index) => index+''}
                            renderItem={({item}) => this.renderNovedad(item)
                            }
                        />
                        {/* </View> */}
                    </ScrollView>
                </View>
                <View style={[ {alignSelf: (this.state.nextButton)?'flex-end':'flex-start', justifyContent: 'flex-end'}]}>
                    {!this.state.nextButton && <BtnAction img={require('../images/btn_volver.png')} onPress={() => Actions.pop({refresh: {setHeader: this.props.setHeader}})}/>}
                    {this.state.nextButton && <BtnAction onPress={()=> Actions.grid_vigilancia()} txt='Siguiente'/>}
                </View>
            </View>
            );
    }

}

const styles= {
    titleText: {
        fontSize: 20,
        // fontWeight: 'bold',
        color: 'white',
        alignSelf: 'center',
        textAlign: 'center',
        fontFamily: "cronos",
    },
    rowText: {
        fontSize: 20,
        color: 'black',
        alignSelf: 'center',
        textAlign: 'center',
        fontFamily: "cronos",
    },
    subContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 8,
        flex: 1,
        backgroundColor: '#415d68',
        height: 35,
        borderWidth: 4,
        borderColor: '#3b464c',
        height: 35
    },
    rowContainer: {
        flexDirection: 'row',
        flex: 1,
        height: 40,
        borderBottomWidth: 3,
        borderColor: 'black',
    },
    otherData: {
        flex: 1,
        borderRightWidth: 2,
        borderColor: '#3b464c',
        height: 35
    },
    mainData: {
        flex: 2,
        borderRightWidth: 2,
        borderColor: '#3b464c',
        height: 35

    },
    otherDataRow: {
        flex: 1,
        height: 35
    },
    mainDataRow: {
        flex: 2,
        height: 35
    },
    row: {
        flexDirection: 'row',
        paddingVertical: 6,
        alignSelf: 'center',
    },
    container:{
        flexDirection: 'column',
        paddingHorizontal: 115,
        paddingTop: 6,
        flex: 1,
    },
    scrollView: {
        flex: 1,
    }
}

const mapStateToProps = (state) => {
    return {usuario: state.usuario};
}

export default connect(mapStateToProps)(Novedades);