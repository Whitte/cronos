import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {BtnAction}  from '../../components/BtnAction';
import {BtnImageText} from "../../components/BtnImageText";
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';

class Mensajeria extends Component{

    constructor(props) {
        super(props);
        this.props.setHeader('Mensajería');
    }

    async onClick(action, payload={}){
        switch(action){
            case 'msj_administrador':
                Actions.administrador();
                break;
            case 'msj_empseguridad':
                Actions.empSeguridad();
                break;
            case 'atras':
                Actions.pop({refresh: {setHeader: this.props.setHeader}});
                break;
        }
    }


    render(){
        return(
            <View style={styles.view}>
                <View style={[styles.footer,{justifyContent: 'center'}]}>
                    <Text style={styles.titleText}>ENVIAR MENSAJE A:</Text>
                </View>
                <View style={styles.main}>
                    <View style={[styles.container, {flexDirection:'row'}]}>
                    </View>
                    <View style={[styles.container, {flexDirection:'row', flex: 3}]}>
                        <View  style={[styles.view, {flexDirection:'row',justifyContent: 'space-between'}]}>
                            <BtnImageText img={require('../../images/buttons/btn_mensaje-administrador.png')} tint='black' txt='Administrador'
                                onPress={() => this.onClick('msj_administrador')}
                            />
                            <BtnImageText img={require('../../images/buttons/btn_mensaje-empseguridad.png')} tint='black' txt='Empresa de seguridad'
                                onPress={() => this.onClick('msj_empseguridad')}
                            />
                        </View>
                    </View>
                    <View style={[styles.container, {flexDirection:'row'}]}>
                    </View>
                </View>
                <View style={styles.footer}>
                    <BtnAction img={require('../../images/btn_volver.png')} onPress={() => this.onClick('atras')}/>
                </View>
            </View>
            );
    }

}

const styles= {
    row: {
        flexDirection: 'row',
        paddingVertical: 6,
        justifyContent: 'space-between'
    },
    main: {
        flexDirection: 'row',
        flex: 5,
        paddingVertical: 3,
        justifyContent: 'space-between',
    },
    footer: {
        flexDirection: 'row',
        flex: 1,
        paddingHorizontal: 15,
        paddingTop: 14,
        paddingBottom: 8,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    view:{
        flexDirection: 'column',
        flex:1,
    },
    container:{
        marginHorizontal: 30,
        justifyContent: 'space-around',
        flex:1,
    },
    titleText:{
        fontSize: 25,
        fontFamily: "cronos",
        alignSelf: 'center',
        color: '#3b464c'
    }
}

const mapStateToProps = (state) => {
    return {usuario: state.usuario.usuario, token: state.usuario.token};
}

export default connect(mapStateToProps)(Mensajeria);