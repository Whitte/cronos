import React, {Component} from 'react';
import {View, Alert, Text} from 'react-native';
import {IconText} from "../../components/IconText";
import {BtnAction}  from '../../components/BtnAction';
import {BtnImageText} from "../../components/BtnImageText";
import moment from 'moment';
import {Actions} from 'react-native-router-flux';
import axios from 'axios';
import qs from 'qs';
import {connect} from 'react-redux';

const route = 'http://api.cronosseguridad.com/';

const novedades = {
    carro: 'NOVEDAD EN CARRO',
    bloque: 'NOVEDAD EN BLOQUE',
    apto: 'NOVEDAD EN APTO',
    otra: 'OTRA NOVEDAD'
};

class Mensajeria extends Component{

    constructor(props) {
        super(props);
        this.props.setHeader('Mensajería');
    }

    async onClick(action, payload={}){
        let res = '';
        try{
            switch(action){
                case 'punto_vigilancia':
                    res = await this.enviarEmail('administrador_punto_vigilancia');
                    if(res.data.status == 200){
                        Alert.alert(res.data.message);
                    }
                    break;
                case 'empresa_seguridad':
                    res = await this.enviarEmail('administrador_empresa_seguridad');
                    if(res.data.status == 200){
                        Alert.alert(res.data.message);
                    }
                    break;
                case 'atras':
                    // Actions.pop({refresh: {setHeader: this.props.setHeader}});
                    break;
            }
            Actions.popTo('grid_vigilancia',{refresh: {setHeader: this.props.setHeader}});
        }
        catch(err){
            console.log(err);
        }
    }

    async enviarEmail(asunto){
        let data = {};
        try{
            data= {
                toid: 'caro9031@gmail.com',
                subject: asunto,
                date: moment().format('DD/MM/YYYY h:mm A'),
                id_edificio: this.props.usuario.data.id_edificio,
                id_personal: this.props.usuario.data.id,
                extra: 'empty'
            };
            data = qs.stringify(data);
            let headers = {
                "Auth-Key": "cronos-ak-v1",
                "X-Cronos-Auth-Token": this.props.token,
                "Content-Type": "application/x-www-form-urlencoded"
            };
            return axios.post(route+'email',
                data, 
                {
                  headers: headers
                });
        }
        catch(err){
            console.log(err)
        }
    }


    render(){
        return(
            <View style={styles.view}>
                <View style={[styles.footer,{justifyContent: 'center'}]}>
                    <Text style={styles.titleText}>ENVIAR MENSAJE A ADMINISTRADOR:</Text>
                </View>
                <View style={[styles.footer,{justifyContent: 'center'}]}>
                    <IconText txt='Seleccione el tipo de mensaje'/>
                </View>
                <View style={styles.main}>
                    <View style={[styles.container, {flexDirection:'row'}]}>
                    </View>
                    <View style={[styles.container, {flexDirection:'row', flex: 10}]}>
                        <View  style={[styles.view, {flexDirection:'row',justifyContent: 'space-between'}]}>
                            <BtnImageText img={require('../../images/buttons/btn_comu-punto-vigilancia.png')} tint='black' txt='Punto de vigilancia'
                                onPress={()=> this.onClick('punto_vigilancia')}
                            />
                            <BtnImageText img={require('../../images/buttons/btn_mensaje-empseguridad.png')} tint='black' txt='Empresa de seguridad'
                                onPress={()=> this.onClick('empresa_seguridad')}
                            />
                            <BtnImageText img={require('../../images/buttons/btn_comu-apto.png')} tint='black' txt='Apartamento'
                                onPress={()=> this.onClick('')}
                            />
                        </View>
                    </View>
                    <View style={[styles.container, {flexDirection:'row'}]}>
                    </View>
                </View>
                <View style={styles.footer}>
                    <BtnAction img={require('../../images/btn_volver.png')} onPress={() => this.onClick('atras')}/>
                </View>
            </View>
            );
    }

}

const styles= {
    row: {
        flexDirection: 'row',
        paddingVertical: 6,
        justifyContent: 'space-between'
    },
    main: {
        flexDirection: 'row',
        flex: 5,
        paddingVertical: 3,
        justifyContent: 'space-between',
    },
    footer: {
        flexDirection: 'row',
        flex: 1.5,
        paddingHorizontal: 15,
        paddingTop: 14,
        paddingBottom: 8,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    view:{
        flexDirection: 'column',
        flex:1,
    },
    container:{
        marginHorizontal: 30,
        justifyContent: 'space-around',
        flex:1,
    },
    titleText:{
        fontSize: 25,
        fontFamily: "cronos",
        alignSelf: 'center',
        color: '#3b464c'
    }
}

const mapStateToProps = (state) => {
    return {usuario: state.usuario.usuario, token: state.usuario.token};
}

export default connect(mapStateToProps)(Mensajeria);