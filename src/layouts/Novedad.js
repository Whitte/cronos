import React, {Component} from 'react';
import {View, Alert, Image, BackHandler} from 'react-native';
import {IconText} from "../components/IconText";
import {BtnAction}  from '../components/BtnAction';
import {CkbImage}  from '../components/CkbImage';
import moment from 'moment';
import {Actions} from 'react-native-router-flux';
import ImgToBase64 from 'react-native-image-base64';
import axios from 'axios';
import qs from 'qs';
import {connect} from 'react-redux';
import * as common from '../common/common';

const route = 'http://api.cronosseguridad.com/';

const novedades = {
    carro: 'NOVEDAD EN CARRO',
    bloque: 'NOVEDAD EN BLOQUE',
    apto: 'NOVEDAD EN APTO',
    otra: 'OTRA NOVEDAD'
};

const INITIAL_STATE = {
    carro: false,
    bloque: false,
    apto: false,
    otra: false,
    foto: '',
    sending: false
};

class Novedad extends Component{

    constructor(props) {
        super(props);
        this.props.setHeader('Novedades');
        this.state =INITIAL_STATE;
    }

    async onClick(action, payload={}){
        if(!this.state.sending)
        switch(action){
            case 'camera':
                Actions.camera({QR: false, getQR: {}, getPicture: this.getRutaImagen.bind(this)});
            break;
            case 'enviarNovedad':
                if(this.getStringNovedad().length < 1){
                    Alert.alert('Debe seleccionar un tipo de novedad');
                }
                else if(this.state.foto.length < 1){
                    Alert.alert('Debe tomar una fotografía de la novedad');
                }
                else{
                    let novedad = await this.getNovedadJSON();
                    let connected = await common.isConnected();
                    if(connected){
                        this.setState({sending: true});
                        console.log(novedad);
                        try{
                            // console.time('novedad');
                            await this.enviarNovedad(novedad);

                            Alert.alert('Novedad enviada');
                            this.setState({...INITIAL_STATE, sending: true});
                            Actions.popTo('grid_vigilancia',{refresh: {setHeader: this.props.setHeader}});
                            // console.timeEnd('novedad');
                        }
                        catch(err){
                            console.log(err);
                            Alert.alert('Falló el envio de la novedad');
                            this.setState({sending: false});
                        }
                    }
                    else{
                        novedad = await this.getNovedadToSave();
                        console.log(novedad);
                        common.addItemTo('novedades', novedad);
                        Alert.alert('Novedad guardada');
                        this.setState(INITIAL_STATE);
                        Actions.pop({refresh: {setHeader: this.props.setHeader}});
                    }
                }
            break;
            case 'rondas':
                Actions.rondas();
            break;
            case 'atras':
                Actions.pop({refresh: {setHeader: this.props.setHeader}});
            break;
        }
    }

    async getNovedadToSave(){
        const novedadJSON = await this.getNovedadJSON();
        return {
            ...novedadJSON,
            imagen: this.state.foto
        }
    }

    async getNovedadJSON(){
        let novedad = {};
        try{
            novedad = {
                descripcion: this.getStringNovedad(),
                image_name: moment().format('YYYY-MM-DD-h-mm-ss'),
                imagen: await ImgToBase64.getBase64String(this.state.foto),
                id_personal: this.props.usuario.usuario.data.id,
                id_edificio: this.props.usuario.usuario.data.id_edificio
            };

        }
        catch(err){
            throw err;
        }
        return novedad;
    }

    getRutaImagen(path){
        this.setState({foto: path});
    }

    getStringNovedad(){
        let novedad = '';
        for(let i in this.state){
            if(this.state[i] && i != 'foto'){
                novedad += novedades[i]+'; ';
            }
        }
        return novedad;
    }

    async enviarEmail(asunto){
        let data = {};
        try{
            data= {
                subject: asunto,
                date: moment().format('DD/MM/YYYY h:mm A'),
                id_edificio: this.props.usuario.usuario.data.id_edificio,
                id_personal: this.props.usuario.usuario.data.id,
                extra: ''
            };
            data = qs.stringify(data);
            let headers = {
                "Auth-Key": "cronos-ak-v1",
                "X-Cronos-Auth-Token": this.props.usuario.token,
                "Content-Type": "application/x-www-form-urlencoded"
            };
            return axios.post(route+'email',
                data,
                {
                  headers: headers
                });
        }
        catch(err){
            console.log(err)
        }
    }

    async enviarNovedad(novedad){
        novedad = qs.stringify(novedad);
        let headers = {
            "Auth-Key": "cronos-ak-v1",
            "X-Cronos-Auth-Token": this.props.usuario.token,
            "Content-Type": "application/x-www-form-urlencoded"
        };
        return axios.post(route+'novedades/create',
            novedad,
            {
              headers: headers
            });
    }

    renderImage(path){
        return(
            (path.length>1)
                ?<Image source={{uri: path}}  style={styles.view}></Image>
                :<View  style={[styles.view, styles.imgContainer]}>
                    <Image resizeMode={'contain'} style={styles.image} source={require('../images/icono_camara.png')}/>
                </View>);
    }

    render(){
        return(
            <View style={styles.view}>
                <View style={[styles.footer, {paddingLeft: '22%'}]}>
                    <IconText txt='Seleccione el tipo de novedad a reportar'/>
                </View>
                <View style={styles.main}>
                    <View style={[styles.container, {flexDirection:'row'}]}>
                        <View  style={[styles.view, {justifyContent: 'space-around'}]}>
                            <CkbImage txt={novedades.carro} img={require('../images/icono_novedad-carro.png')}
                                check={this.state.carro} onPress={()=>{ this.setState({carro: !this.state.carro})}} />
                            <CkbImage txt={novedades.bloque} img={require('../images/icono_novedad-bloque.png')}
                                check={this.state.bloque} onPress={()=>{ this.setState({bloque: !this.state.bloque})}} />
                            <CkbImage txt={novedades.apto} img={require('../images/icono_novedad-apto.png')}
                                check={this.state.apto} onPress={()=>{ this.setState({apto: !this.state.apto})}} />
                            <CkbImage txt={novedades.otra} img={require('../images/icono_otra-novedad.png')}
                                check={this.state.otra} onPress={()=>{ this.setState({otra: !this.state.otra})}} />
                        </View>
                        <View  style={[styles.view]}>
                            <View style={styles.main}>
                                <View style={[styles.container, {flexDirection:'row'}]}>
                                    { this.renderImage(this.state.foto) }
                                </View>
                            </View>
                            <View style={styles.footer}>
                                <BtnAction txt='Tomar foto' img={require('../images/btn_tomar-foto.png')} onPress={() => this.onClick('camera')}/>
                                <BtnAction txt='Enviar' img={require('../images/btn_enviar.png')} onPress={() => this.onClick('enviarNovedad')}/>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.footer}>
                    <BtnAction img={require('../images/btn_volver.png')} onPress={() => this.onClick('atras')}/>
                    <BtnAction txt='Volver a la ronda' onPress={() => this.onClick('rondas')}/>
                </View>
            </View>
            );
    }

}

const styles= {
    row: {
        flexDirection: 'row',
        paddingVertical: 6,
        justifyContent: 'space-between'
    },
    main: {
        flexDirection: 'row',
        flex: 5,
        paddingVertical: 3,
        justifyContent: 'space-between',
    },
    footer: {
        flexDirection: 'row',
        flex: 1,
        paddingHorizontal: 15,
        paddingTop: 14,
        paddingBottom: 8,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    view:{
        flexDirection: 'column',
        flex:1,
    },
    container:{
        marginHorizontal: 30,
        justifyContent: 'space-around',
        flex:1,
    },
    list: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    imgContainer: {
        backgroundColor: '#3b464c',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent:'center'
    },
    image: {
        width: '40%',
        tintColor: 'white'
    }
}

const mapStateToProps = (state) => {
    return {usuario: state.usuario};
}

export default connect(mapStateToProps)(Novedad);