import React, {Component} from 'react';
import {View,TouchableOpacity, Alert, Image} from 'react-native';
import {BtnAction}  from '../components/BtnAction';
import {CkbImage}  from '../components/CkbImage';
import {TextRectBg}  from '../components/TextRectBg';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';

import ImgToBase64 from 'react-native-image-base64';
import moment from 'moment';
import axios from 'axios';
import qs from 'qs';
import * as actions from '../actions';
import { pushNotifications } from '../services';

const route = 'http://api.cronosseguridad.com/';

const INITIAL_STATE= {
    tipo: {
        visitante: false,
        domiciliario: false,
        contratista: false,
        domestico: false
    },
    imagen: '',
    bloque: {id: '', nombre_bloque: ''},
    apto: {id: '', nombre_apto: ''},
    sending: false
};

class RegistroVisitantes extends Component{

    constructor(props) {
        super(props);
        this.props.setHeader('Registro de visitantes');
        this.state = INITIAL_STATE;
    }

    recibirBloqueyApto(bloque, apto) {
        this.setState({
            bloque: bloque,
            apto: apto
        });
    }

    async onClick(action, payload={}){
        const { registrarVisitante, setHeader } = this.props;
        const { imagen, tipo, apto, sending } = this.state;
        if(!sending)
        switch(action){
            case 'camera':
                Actions.camera({QR: false, getQR: {}, getPicture: this.getRutaImagen.bind(this)});
            break;
            case 'registrar_ingreso':
                if(this.getTipoVisitante() == null){
                    Alert.alert('Debe seleccionar un tipo de visitante');
                }
                else if(apto.id == ''){
                    Alert.alert('Debe seleccionar un bloque y apartamento');
                }
                else if(imagen == ''){
                    Alert.alert('Debe tomar una foto del visitante');
                }
                else{
                    this.setState({sending: true});
                    try{
                        let idVisitante = await this.registrarIngreso();
                        idVisitante = idVisitante.data.id_visitante;
                        if(tipo.domiciliario){
                            let now5 = moment().add(5 ,'minutes').toDate();
                            pushNotifications.localNotificationSchedule(idVisitante,'Alerta de visitante '+idVisitante,'El domiciliario aún no ha salido '+now5, now5);
                            registrarVisitante({id: idVisitante, imagen: imagen});
                        }

                        registrarVisitante({id: idVisitante, imagen});
                        Alert.alert('Ingreso registrado');
                        this.setState({...INITIAL_STATE});
                        Actions.pop({refresh: {setHeader}});
                    }
                    catch(err){                        
                        this.setState({sending: false});
                    }
                }
            break;
            case 'registrar_salida':
                Actions.registrarSalida();
            break;
            case 'atras':
                Actions.pop({refresh: {setHeader: this.props.setHeader}});
            break;
        }
    }

    getTipoVisitante(){
        if(this.state.tipo.visitante){
            return 1;
        }
        else if(this.state.tipo.domiciliario){
            return 0;
        }
        else if(this.state.tipo.domestico){
            return 2;
        }
        else if(this.state.tipo.contratista){
            return 3;
        }
        else{
            return null;
        }
    }

    async registrarIngreso(){
        let data = {};
        try{
            data= {
                tipo_visitante: this.getTipoVisitante(),
                imagen: await ImgToBase64.getBase64String(this.state.imagen),
                image_name: moment().format('YYYY-MM-DD-h-mm-ss'),
                id_apartamento: this.state.apto.id,
                id_vigilante: this.props.usuario.data.id
            };
            data = qs.stringify(data);
        }
        catch(err){
            console.log(err)
        }
        let headers = {
            "Auth-Key": "cronos-ak-v1",
            "X-Cronos-Auth-Token": this.props.token,
            "Content-Type": "application/x-www-form-urlencoded"
        };
        return axios.post(route+'visitantes/create',
            data,
            {
              headers: headers
            });
    }

    getRutaImagen(path){
        this.setState({imagen: path});
    }

    renderImage(path){
        return(
            (path.length>1)
                ?<Image source={{uri: path}}  style={styles.view}></Image>
                :<View  style={[styles.view, styles.imgContainer]}>
                    <Image resizeMode={'contain'}  style={styles.image} source={require('../images/icono_camara.png')}/>
                </View>);
    }

    render(){
        return(
            <View style={styles.view}>

                <View style={styles.main}>
                    <View style={[styles.container, {flexDirection:'row'}]}>
                        <View  style={[styles.view, {justifyContent: 'space-around'}]}>
                            <CkbImage txt="Visitante" img={require('../images/icono_visitante.png')}
                                check={this.state.tipo.visitante} onPress={()=>{ this.setState({tipo: {visitante: !this.state.visitante}})}} ></CkbImage>
                            <CkbImage txt="Domiciliario" img={require('../images/icono_domiciliario.png')}
                                check={this.state.tipo.domiciliario} onPress={()=>{ this.setState({tipo: {domiciliario: !this.state.domiciliario}})}} ></CkbImage>
                            <CkbImage txt="Servicio Doméstico" img={require('../images/icono_servicio_domestico.png')}
                                check={this.state.tipo.domestico} onPress={()=>{ this.setState({tipo: {domestico: !this.state.domestico}})}} ></CkbImage>
                            <CkbImage txt="Contratista" img={require('../images/icono_contratista.png')}
                                check={this.state.tipo.contratista} onPress={()=>{ this.setState({tipo: {contratista: !this.state.contratista}})}} ></CkbImage>
                            <View style={styles.rectTextContainer}>
                                <TouchableOpacity onPress ={() => Actions.bloques({recibirBloqueyApto: this.recibirBloqueyApto.bind(this)})}>
                                    <TextRectBg bgColor='#dadada' label='Bloque' value={this.state.bloque.nombre_bloque}/>
                                </TouchableOpacity>
                                <TextRectBg bgColor='#b7b7b6' label='Apartamento' value={this.state.apto.nombre_apto}></TextRectBg>
                            </View>
                        </View>
                        <View  style={[styles.view]}>
                            <View style={styles.main}>
                                <View style={[styles.container, {flexDirection:'row'}]}>
                                        { this.renderImage(this.state.imagen) }
                                </View>
                            </View>
                            <View style={styles.footer}>
                                <BtnAction txt='Tomar foto' img={require('../images/btn_tomar-foto.png')} onPress={() => this.onClick('camera')}/>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.footer}>
                    <BtnAction img={require('../images/btn_volver.png')} onPress={() => Actions.pop({refresh: {setHeader: this.props.setHeader}})}/>
                    <BtnAction txt='Registrar ingreso' img={require('../images/bnt_ingreso.png')} onPress ={()=> this.onClick('registrar_ingreso')}/>
                    <BtnAction txt='Registrar salida' img={require('../images/registrar_salida.png')} onPress ={()=> this.onClick('registrar_salida')}/>
                </View>
            </View>
            );
    }

}

const styles= {
    row: {
        flexDirection: 'row',
        paddingVertical: 6,
        justifyContent: 'space-between'
    },
    main: {
        flexDirection: 'row',
        flex: 5,
        paddingVertical: 3,
        justifyContent: 'space-between',
    },
    footer: {
        flexDirection: 'row',
        flex: 1,
        paddingHorizontal: 15,
        paddingTop: 14,
        paddingBottom: 8,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    view:{
        flexDirection: 'column',
        flex:1,
    },
    container:{
        marginHorizontal: 30,
        justifyContent: 'space-between',
        flex:1,
    },
    rectTextContainer: {
        marginTop: 5,
        borderTopWidth: 5,
        borderColor: '#415d68',
        paddingTop: 2,
        alignSelf: 'flex-end',
        width: '70%',
        justifyContent: 'space-between',
    },
    imgContainer: {
        backgroundColor: '#3b464c',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent:'center'
    },
    image: {
        width: '40%',
        tintColor: 'white'
    }
}

const mapStateToProps = (state) =>{
    return {usuario: state.usuario.usuario, token: state.usuario.token};
};

export default connect(mapStateToProps, actions)(RegistroVisitantes);