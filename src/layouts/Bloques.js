import React, {Component} from 'react';
import {View,Text, Image, FlatList, TouchableOpacity} from 'react-native';
import {IconText} from "../components/IconText";
import {BtnAction}  from '../components/BtnAction';
import axios from 'axios';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import * as common from '../common/common';

const route = 'http://api.cronosseguridad.com/';


class Corespondencia extends Component{

    constructor(props) {
        super(props);
        this.state = {
            selecting: 'bloque',
            bloque: {id:'', nombre_bloque:''},
            apto: {id:'', nombre_apto:''},
            bloques: [{id:'', nombre_bloque: ''}],
            aptos: [{id:'', nombre_apto: ''}]
        };
    }

    async componentWillMount(){
        try{
            let bloques = await common.getItem('bloques');
            this.setState({bloques});
            // await this.requestBloques(this.props.usuario.data.id_edificio);
            // if(bloques.data.status == 200){
            //     this.setState({
            //         bloques: bloques.data.result
            //     });
            // }
            // else{
            //     throw "Error en la app";
            // }
        }
        catch(err){
            console.log(err);
        }
    }

    async requestAptos(idBloque){
        return axios({
            method: 'GET',
            url: route+'apartamentos/'+idBloque,
            headers: {
                "Auth-Key": "cronos-ak-v1",
                "X-Cronos-Auth-Token": this.props.token,
                }
          });
    }

    onClick(action, payload={}){
        switch(action){
            case 'enviar':
                if(this.state.bloque.id != '' && this.state.apto.id != ''){
                    this.props.recibirBloqueyApto(this.state.bloque,this.state.apto);
                    Actions.pop({refresh: {setHeader: this.props.setHeader}});
                }
            break;
            case 'atras':
            if(this.state.selecting == 'bloque'){
                Actions.pop({refresh: {setHeader: this.props.setHeader}});
            }
            else{
                this.setState({
                    selecting: 'bloque', 
                    aptos:[{id:'', nombre_apto:''}], 
                    apto: {id:'', nombre_apto:''}
                });
            }
        }
    }

    selectBloque(rowData){        
        if(this.state.selecting == 'bloque'){
            this.setState({bloque:rowData,selecting: 'apto'}, async ()=>{
                console.log(rowData);
                this.setState({
                    aptos: rowData.aptos
                });
            });
        }
        else{
            this.setState({apto:rowData})
        }
    }

    render(){
        return(
            <View style={styles.view}>
                <View style={[styles.footer, {alignSelf: 'flex-start', width: '65%',marginHorizontal: 160, borderBottomColor:'#415d68', borderBottomWidth: 3}]}>
                    <Image  resizeMode={'contain'} style={[styles.image,{marginRight: '25%'}]}
                        source={(this.state.selecting == 'bloque')?require('../images/icono_novedad-bloque.png'):require('../images/icono_novedad-apto.png')}
                    />
                    <IconText style={{}} txt={(this.state.selecting == 'bloque')?'Seleccione el bloque':'Seleccione el apto'}/>
                </View>
                <View style={styles.main}>
                    <View style={[styles.container, {flexDirection:'row',marginHorizontal: 160}]}>
                        <FlatList contentContainerStyle={styles.list}
                                initialListSize={20}
                                numColumns={5}
                                keyExtractor={(item, index) => index}
                                data={(this.state.selecting == 'bloque')?this.state.bloques:this.state.aptos}
                                renderItem={({item}) => 
                                    <TouchableOpacity style={styles.item} onPress={()=> this.selectBloque(item)}>
                                        <Text style={{fontSize: 20, fontWeight: 'bold'}}>{(this.state.selecting == 'bloque')?item.nombre_bloque:item.nombre_apto}</Text>
                                    </TouchableOpacity>
                                    }
                                />
                    </View>
                </View>
                <View style={[styles.footer, {marginBottom: 20}]}>
                    <View style={{paddingTop:8}}>
                        <BtnAction style={{flex:1, paddingTop: 8,}}  onPress = {() => this.onClick('atras')}  txt='Atras'/>
                    </View>
                    <View style={{flex:1, flexDirection: 'row', height: '100%', paddingHorizontal:12, marginHorizontal:12}}>
                        <View style={[styles.txtContainer, {backgroundColor: '#dadada',}]}>
                            <Text style={styles.text}>{'BLOQUE: '+this.state.bloque.nombre_bloque}</Text>
                        </View>
                        <View style={[styles.txtContainer, {backgroundColor: '#b7b7b6',}]}>
                            <Text style={styles.text}>{'APARTAMENTO: '+this.state.apto.nombre_apto}</Text>
                        </View>
                    </View>
                    <View style={{paddingTop:8}}>
                        <BtnAction style={{flex:1, paddingTop: 8}} onPress = {() => this.onClick('enviar')} txt='Enviar'/>
                    </View>
                </View>
            </View>
            );
    }

}

const styles= {
    row: {
        flexDirection: 'row',
        paddingVertical: 6,
        justifyContent: 'space-between'
    },
    main: {
        flexDirection: 'row',
        flex: 5,
        paddingVertical: 3,
        justifyContent: 'space-between',
    },
    footer: {
        flexDirection: 'row',
        flex: 1,
        paddingHorizontal: 15,
        paddingTop: 14,
        paddingBottom: 8,
        alignItems: 'center'
    },
    view:{
        flexDirection: 'column',
        flex:1,
    },
    container:{
        marginHorizontal: 30,
        justifyContent: 'space-around',
        flex:1,
    },
    rectTextContainer: {
        borderTopWidth: 5,
        borderColor: '#415d68',
        paddingTop: 10,
        alignSelf: 'flex-end', 
        width: '70%', 
        justifyContent: 'space-between',
    },
    text:{
        color: '#3b464c',
        fontSize: 22,
        fontWeight: 'bold',
        alignSelf: 'flex-start',
    },
    txtContainer:{
        alignItems: 'center',
        height: 55,
        flexDirection: 'column',
        paddingHorizontal: 10,
        justifyContent: 'space-around',
        marginVertical: 5,
        width: '50%'
    },
    image:{
        height: 40,
        width: 40,
        tintColor: '#415d68',
    },
    list: {
        flexDirection: 'column',
    },
    item:{
        borderColor: '#415d68',
        borderWidth: 3,
        margin: 10,
        width: 100,
        height: 70,
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: '#dadada'
    }
}
const mapStateToProps = (state) =>{
    return {
        usuario: state.usuario.usuario,
        token: state.usuario.token
    };
}

export default connect(mapStateToProps)(Corespondencia);