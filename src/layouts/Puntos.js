import React, {Component} from 'react';
import {View, FlatList} from 'react-native';
import {BtnAction}  from '../components/BtnAction';
import BtnPunto from '../components/BtnPunto';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';

class Puntos extends Component{

    constructor(props) {
        super(props);
        this.state ={
            puntos: this.props.puntos
        }
    }

    render(){
        return(
            <View style={styles.view}>
                <View style={styles.main}>
                    <View style={styles.container}>
                            <FlatList contentContainerStyle={styles.list} 
                                numColumns={4}
                                keyExtractor={(item, index) => index}
                                data={this.state.puntos}
                                renderItem=
                                {({item}) =>
                                    <BtnPunto  actualizarPuntoHecho={() => this.props.actualizarPuntoHecho()} style={styles.item} punto={item}/>
                                }
                            />
                    </View>
                </View>
                <View style={styles.footer}>
                    <BtnAction img={require('../images/btn_volver.png')} onPress= {() => Actions.pop({refresh: {setHeader: this.props.setHeader}})}/>
                    <BtnAction txt='Reportar novedad' onPress={() => Actions.novedad()}/>
                </View>
            </View>
            );
    }

}

const styles= {
    row: {
        flexDirection: 'row',
        paddingVertical: 6,
        justifyContent: 'space-between'
    },
    main: {
        flexDirection: 'row',
        flex: 5,
        paddingVertical: 3,
        justifyContent: 'space-between',
    },
    footer: {
        flexDirection: 'row',
        flex: 1,
        paddingHorizontal: 15,
        paddingVertical: 8,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    view:{
        flexDirection: 'column',
        justifyContent: 'space-around',
        flex:1,
    },
    container:{
        marginHorizontal: 115,
        justifyContent: 'space-around',
        flex:1,
    },
    list: {
        flexDirection: 'column',
    }
}
const mapStateToProps = (state) => {
    return {puntos: state.puntos.puntos};
}

export default connect(mapStateToProps)(Puntos);