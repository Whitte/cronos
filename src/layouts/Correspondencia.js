import React, {Component} from 'react';
import {View, Image, Alert} from 'react-native';
import {IconText} from "../components/IconText";
import {BtnAction}  from '../components/BtnAction';
import {CkbImage}  from '../components/CkbImage';
import {TextRectBg}  from '../components/TextRectBg';
import {Actions} from 'react-native-router-flux';
import moment from 'moment';
import axios from 'axios';
import qs from 'qs';
import {connect} from 'react-redux';

const route = 'http://api.cronosseguridad.com/';

const INITIAL_STATE= {
    admin: false,
    gas: false,
    luz: false,
    agua: false
};

class Corespondencia extends Component{

    constructor(props) {
        super(props); 
        this.props.setHeader('Correspondencia');
        this.state =INITIAL_STATE;
    }

    async recibirBloqueyApto(bloque, apto){
        let data = {};
        try{
            data= {
                subject: 'correspondencia personal',
                date: moment().format('DD/MM/YYYY h:mm A'),
                id_edificio: this.props.usuario.data.id_edificio,
                id_personal: this.props.usuario.data.id,
                id_apartamento: apto.id,
                extra: ''
            };            
            let res = await this.enviarCorrespondencia(data);
            if(res.data.status == 200){
                Alert.alert(res.data.message);
                Actions.popTo('grid_vigilancia',{refresh: {setHeader: this.props.setHeader}});
            }
        }
        catch(err){
            console.log(err);
        }
    }

    getExtra(){
        let extra= '';
        for(let i in this.state){
            extra += (this.state[i])?i+',':'';
        }
        return extra.substring(0, extra.length-1);
    }

    async onClick(action, payload={}){
        switch(action){
            case 'bloques':
                Actions.bloques({recibirBloqueyApto: this.recibirBloqueyApto.bind(this)});
            break;
            case 'todos':
                console.log('corres todos');
                let data = {};
                if(this.getExtra().length > 1){
                    try{
                        data= {
                            toid: 'caro9031@gmail.com ',
                            subject: 'correspondencia',
                            date: moment().format('DD/MM/YYYY h:mm A'),
                            id_edificio: this.props.usuario.data.id_edificio,
                            id_personal: this.props.usuario.data.id,
                            extra: this.getExtra()
                        };
                        let res = await this.enviarCorrespondencia(data);
                        // if(res.data.status == 200){
                            // Alert.alert(res.data.message);
                            Alert.alert('Email enviado');
                            this.setState(INITIAL_STATE);
                            Actions.popTo('grid_vigilancia',{refresh: {setHeader: this.props.setHeader}});
                            console.log(res);
                        // }
                    }
                    catch(err){
                        console.log(err);
                    }
                }
                else{
                    Alert.alert('Debe seleccionar un tipo de correspondencia');
                }
            break;
            case 'atras':
                Actions.pop({refresh: {setHeader: this.props.setHeader}});
            break;
        }
    }

    async enviarCorrespondencia(data){
        data = qs.stringify(data);
        let headers = {
            "Auth-Key": "cronos-ak-v1",
            "X-Cronos-Auth-Token": this.props.token,
            "Content-Type": "application/x-www-form-urlencoded"
        };
        return axios.post(route+'email',
            data, 
            {
              headers: headers
            });
    }

    render(){
        return(
            <View style={styles.view}>
                <View style={[styles.footer, {paddingLeft: '22%'}]}>
                    <IconText txt='Seleccione el tipo de correspondencia'/>
                </View>
                <View style={styles.main}>
                    <View style={[styles.container, {flexDirection:'row'}]}>
                        <View  style={[styles.view, {justifyContent: 'space-around'}]}>
                            <CkbImage txt="Agua" img={require('../images/icono_agua.png')} 
                                check={this.state.agua} onPress={()=>{ this.setState({agua: !this.state.agua})}} ></CkbImage>
                            <CkbImage txt="Luz" img={require('../images/icono_luz.png')}
                                check={this.state.luz} onPress={()=>{ this.setState({luz: !this.state.luz})}} ></CkbImage>
                            <CkbImage txt="Gas" img={require('../images/icono_gas.png')} 
                                check={this.state.gas} onPress={()=>{ this.setState({gas: !this.state.gas})}} ></CkbImage>
                            <CkbImage txt="Administracion" img={require('../images/icono_administracion.png')}
                                check={this.state.admin} onPress={()=>{ this.setState({admin: !this.state.admin})}} ></CkbImage>
                        </View>
                        <View  style={[styles.view]}>
                            <View style={styles.main}>
                                <View style={[styles.container, {flexDirection:'row'}]}>
                                <View  style={[styles.view, styles.imgContainer]}>
                                    <Image resizeMode={'contain'} style={styles.image} source={require('../images/icono_correspondencia.png')}/>
                                </View>
                                </View>
                            </View>
                            <View style={[styles.footer,{alignSelf: 'center'}]}>
                                <TextRectBg style={{alignSelf: 'center', width: '100%'}} bgColor='#dadada' label='' value='Paquetes y correspondencia'></TextRectBg>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.footer}>
                    <BtnAction img={require('../images/btn_volver.png')} onPress={() => this.onClick('atras')}/>
                    <BtnAction txt='Enviar a todos' img={require('../images/btn_enviar.png')} onPress= {() => this.onClick('todos')}/>
                    <BtnAction txt='Seleccionar apartamento' onPress={() => {this.onClick('bloques')}}/>
                </View>
            </View>
            );
    }

}

const styles= {
    row: {
        flexDirection: 'row',
        paddingVertical: 6,
        justifyContent: 'space-between'
    },
    main: {
        flexDirection: 'row',
        flex: 5,
        paddingVertical: 3,
        justifyContent: 'space-between',
    },
    footer: {
        flexDirection: 'row',
        flex: 1,
        paddingHorizontal: 15,
        paddingTop: 14,
        paddingBottom: 8,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    view:{
        flexDirection: 'column',
        flex:1,
    },
    container:{
        marginHorizontal: 30,
        justifyContent: 'space-around',
        flex:1,
    },
    rectTextContainer: {
        borderTopWidth: 5,
        borderColor: '#415d68',
        paddingTop: 10,
        alignSelf: 'flex-end', 
        width: '70%', 
        justifyContent: 'space-between',
    },
    imgContainer: {
        backgroundColor: '#3b464c', 
        borderRadius: 10, 
        alignItems: 'center', 
        justifyContent:'center'
    },
    image: {
        width: '40%', 
        tintColor: 'white'
    }
}

const mapStateToProps = (state) => {
    return {
        usuario: state.usuario.usuario,
        token: state.usuario.token
    };
};

export default connect(mapStateToProps)(Corespondencia);