import React, {Component} from 'react';
import {View, Alert, Image, Text, TouchableOpacity} from 'react-native';
import {BtnAction}  from '../components/BtnAction';
import {Actions} from 'react-native-router-flux';
import axios from 'axios';
import qs from 'qs';
import {connect} from 'react-redux';
import * as actions from '../actions';
import { pushNotifications } from '../services';

const route = 'http://api.cronosseguridad.com/';

class SalidaVisitante extends Component{

    constructor(props) {
        super(props);
        this.props.setHeader('REGISTRO DE VISITANTES');
        this.state ={
            bloque: {id: '', nombre_bloque: ''},
            apto: {id: '', nombre_apto: ''},
            imagen: (this.props.visitantes[0] !== undefined)?this.props.visitantes[0].imagen : '',
            index: 0,
            visitantes: this.props.visitantes
        }
    }

    async onClick(action, payload={}){
        switch(action){
            case 'registrar_salida':
                try{
                    let res = await this.registrarSalida(this.state.index);
                    if(res.data.status == 200){
                        pushNotifications.cancelLocalNotification(this.state.visitantes[this.state.index].id);
                        Alert.alert(res.data.message);
                        let visitantes = [...this.state.visitantes];
                        visitantes.splice(this.state.index,1);
                        this.setState({visitantes: visitantes, index: 0, imagen: (visitantes[0] !== undefined)?visitantes[0].imagen : ''}, ()=>{
                            this.props.registrarSalida(this.state.index);
                        });
                    }
                    else{
                        Alert.alert('Ocurrio algo, intenta más tarde');
                    }
                }
                catch(err){
                    console.log(err);
                }
            break;
            case 'anterior':
                let aux1 =this.state.index >0;
                if(aux1){
                    let index = this.state.index;
                    --index;
                    this.setState({index: index, imagen: this.state.visitantes[index].imagen});
                }
            break;
            case 'siguiente':
                let aux2 = this.state.index< (this.state.visitantes.length-1);
                if(aux2){
                    let index = this.state.index;
                    ++index;
                    this.setState({index: index, imagen: this.state.visitantes[index].imagen});
                }
            break;
            case 'buscar':
            if(this.state.apto !== undefined){
                try{
                    let visitantes = await this.visitantesPorApto(this.state.apto.id);
                    console.log(visitantes);
                    if(visitantes.data.status == 200 && visitantes.data.result !== undefined){
                        this.setState({visitantes: visitantes.data.result, index: 0, imagen:(visitantes.data.result[0] !== undefined)?visitantes.data.result[0].imagen : ''}, () =>{
                            console.log(this.state);
                        }
                    );
                    }
                    else{
                        Alert.alert('Sin resultados');
                    }
                }
                catch(err){
                    console.log(err);
                }
            }
            else{
                Alert.alert('Debe seleccionar un apartamento');
            }
            break;
            case 'atras':
                Actions.pop({refresh: {setHeader: this.props.setHeader}});
            break;
        }
    }

    async registrarSalida(i){
        let data = {};
        try{
            data= {
                id_visitante:this.state.visitantes[i].id,
                id_vigilante: this.props.usuario.data.id
            };
            data = qs.stringify(data);
        }
        catch(err){
            console.log(err)
        }
        let headers = {
            "Auth-Key": "cronos-ak-v1",
            "X-Cronos-Auth-Token": this.props.token,
            "Content-Type": "application/x-www-form-urlencoded"
        };
        return axios.post(route+'visitantes/salida',
            data, 
            {
              headers: headers
            });
    }

    async visitantesPorApto(idApto) {
        let leroute = route+'visitantes/'+idApto;
        console.log(leroute);
        return axios({
            method: 'GET',
            url: leroute,
            headers: {
                "Auth-Key": "cronos-ak-v1",
                "X-Cronos-Auth-Token": this.props.token,
                }
          });
    }

    renderImage(path){
        return(
            (path.length>1)
                ?<Image source={{uri: path}}  style={styles.view}></Image>
                :<View  style={[styles.view, {backgroundColor: 'white', borderRadius: 10}]}></View>);
    }

    recibirBloqueyApto(bloque, apto) {
        this.setState({
            bloque: bloque,
            apto: apto
        });
    }

    render(){
        return(
            <View style={styles.view}>
                <View style={[styles.footer,{justifyContent: 'center', alignItems: 'stretch'}]}>
                    <TouchableOpacity  style={[styles.txtContainer, {backgroundColor: '#dadada',}]} onPress={()=> Actions.bloques({recibirBloqueyApto: this.recibirBloqueyApto.bind(this)})}>
                        <View style={{alignSelf: 'flex-start'}}>
                            <Text style={styles.text}>{'BLOQUE: '+ this.state.bloque.nombre_bloque}</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={[styles.txtContainer, {backgroundColor: '#b7b7b6',}]}>
                        <Text style={styles.text}>{'APARTAMENTO: '+this.state.apto.nombre_apto}</Text>
                    </View>
                    <View style={{paddingTop: 10, marginLeft: 15}}>
                        <BtnAction txt='Buscar'  onPress={() => this.onClick('buscar')}/>
                    </View>
                </View>
                <View style={styles.main}>
                    <View style={[styles.container, {flexDirection:'row'}]}>
                        <View  style={[styles.view, {justifyContent: 'space-around'}]}>
                            <TouchableOpacity onPress={() => this.onClick('anterior')} style={{alignSelf: 'center'}}>
                                <Image source={require('../images/flecha_izq.png')}/>
                            </TouchableOpacity>
                        </View>
                        <View  style={[{flex:4}]}>
                            <View style={styles.main}>
                                <View style={[styles.container, {flexDirection:'row'}]}>
                                    { this.renderImage(this.state.imagen) }
                                </View>
                            </View>
                        </View>
                        <View  style={[styles.view, {justifyContent: 'space-around'}]}>
                            <TouchableOpacity onPress={() => this.onClick('siguiente')} style={{alignSelf: 'center'}}>
                                <Image source={require('../images/flecha_der.png')}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={styles.footer}>
                    <BtnAction img={require('../images/btn_volver.png')} onPress={() => this.onClick('atras')}/>
                    <View>
                        <Text style={styles.text}>{(this.state.visitantes.length>0)?this.state.index+1:0}/{this.state.visitantes.length}</Text>
                    </View>
                    <BtnAction txt='Registrar salida' img={require('../images/registrar_salida.png')} onPress={() => this.onClick('registrar_salida')}/>
                </View>
            </View>
            );
    }

}

const styles= {
    row: {
        flexDirection: 'row',
        paddingVertical: 6,
        justifyContent: 'space-between'
    },
    main: {
        flexDirection: 'row',
        flex: 5,
        paddingVertical: 3,
        justifyContent: 'space-between',
    },
    footer: {
        flexDirection: 'row',
        flex: 1,
        paddingHorizontal: 15,
        paddingTop: 14,
        paddingBottom: 8,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    view:{
        flexDirection: 'column',
        flex:1,
    },
    container:{
        marginHorizontal: 30,
        justifyContent: 'space-around',
        flex:1,
    },
    list: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    txtContainer:{
        alignItems: 'center',
        height: 55,
        flexDirection: 'column',
        paddingHorizontal: 10,
        justifyContent: 'space-around',
        marginVertical: 5,
        width: '25%'
    },
    text:{
        color: '#3b464c',
        fontSize: 22,
        fontWeight: 'bold',
        alignSelf: 'flex-start',
    }
}

const mapStateToProps = (state) => {
    return {usuario: state.usuario.usuario, token: state.usuario.token, visitantes: state.visitantes.visitantes};
}

export default connect(mapStateToProps,actions)(SalidaVisitante);