import React, {Component} from 'react';

import {
    Scene, 
    Router, 
    Actions
} from 'react-native-router-flux';

import {View} from 'react-native';
import Header from '../components/header';
import GridVigilancia from './GridVigilancia';
import Rondas from './Rondas';
import Puntos from './Puntos';
import Novedades from './Novedades';
import Novedad from './Novedad';
import RegistroVisitantes from './RegistroVisitantes';
import Correspondencia from './Correspondencia';
import Bloques from './Bloques';
import {ImageView} from '../components/ImageView';
import RNCamera from '../components/RNCamera';
import SalidaVisitantes from './SalidaVisitante';
import Mensajeria from './mensajeria/Mensajeria';
import Administrador from './mensajeria/Administrador';
import EmpSeguridad from './mensajeria/EmpSeguridad';

class Main extends Component {

    constructor(props){
        super(props);
        this.state ={
            title: 'MÓDULO DE VIGILANCIA',
        }
    }

    setHeader(title){
        this.setState({title: title.toUpperCase()});
    }

    render() {
        return (
            <View style={styles.container}>
                <Header sesion={()=> this.props.sesion()} title={this.state.title}></Header>
                <View style={styles.container}>
                    <Router scenes={scenes} setHeader={this.setHeader.bind(this)} hideNavBar/>
                </View>
            </View>
        );
      }
}


export default Main;

const scenes = Actions.create(
    <Scene key='mainRoot'>
        <Scene key='camera' component={RNCamera}/>
        <Scene key="grid_vigilancia" component={GridVigilancia} initial/>
        <Scene key="novedades" component={Novedades}/>
        <Scene key="imageView" component={ImageView}/>
        <Scene key="novedad" component={Novedad}/>
        <Scene key="rondas" component={Rondas}/>
        <Scene key="puntos" component={Puntos}/>
        <Scene key="registroVisitantes" component={RegistroVisitantes}/>
        <Scene key="correspondencia" component={Correspondencia}/>
        <Scene key="bloques" component={Bloques}/>
        <Scene key="registrarSalida" component={SalidaVisitantes}/>
        <Scene key="mensajeria" component={Mensajeria}/>
        <Scene key="administrador" component={Administrador}/>
        <Scene key="empSeguridad" component={EmpSeguridad}/>
    </Scene>
);

const styles = {
    container:{
        flex: 1,        
        flexDirection: 'column',
    },
   
};