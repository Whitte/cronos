import React, {Component} from 'react';
import {View} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {BtnImageText} from "../components/BtnImageText";

class GridVigilancia extends Component{

    constructor(props){
        super(props);
        this.props.setHeader('MÓDULO DE VIGILANCIA');
    }

    componentWillReceiveProps(){
        this.props.setHeader('MÓDULO DE VIGILANCIA');
    }

    actionRouter(action, payload={}){
        switch(action){
            case 'rondas':
                Actions.rondas();
            break;
            case 'novedad':
                Actions.novedad();
            break;
            case 'correspondencia':
                Actions.correspondencia();
            break;
            case 'visitantes':
                Actions.registroVisitantes();
            break;
            case 'mensajeria':
                Actions.mensajeria();
            break;
            case 'novedades':
                Actions.novedades({nextButton: false});
            break;
        }
    }

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.row}>
                    <BtnImageText img={require('../images/buttons/icn_rondas.png')} tint='black' txt='Rondas'
                        onPress={() => this.actionRouter('rondas')}
                    />
                    <BtnImageText img={require('../images/buttons/icn_novedades.png')} tint='black' txt='Novedades'
                        onPress={() => this.actionRouter('novedad')}
                    />
                    <BtnImageText img={require('../images/buttons/icn_correspondencia.png')} tint='black' txt='Correspondencia'
                        onPress={() => this.actionRouter('correspondencia')}
                    />
                </View>
                <View style={styles.row}>
                    <BtnImageText img={require('../images/buttons/icn_visitantes.png')} tint='black' txt='Registro visitantes'
                        onPress={() => this.actionRouter('visitantes')}
                    />
                    <BtnImageText img={require('../images/buttons/icn_chat.png')} tint='black' txt='Mensajeria/Chat'
                        onPress={() => this.actionRouter('mensajeria')}
                    />
                    <BtnImageText img={require('../images/buttons/icn_turno-anterior.png')} tint='black' txt='Turno anterior'
                        onPress={() => this.actionRouter('novedades')}
                    />
                </View>
            </View>
            );
    }

}

const styles= {
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    container:{
        flexDirection: 'column',
        paddingHorizontal: 115,
        justifyContent: 'space-around',
        paddingVertical: 6,
        flex:1,
    },
}

export default GridVigilancia;