import React, {Component} from 'react';

import {View, Alert, AsyncStorage} from 'react-native';
import {ImgButton} from '../common/index';
import axios from 'axios';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import * as actions from '../actions';
import * as common from '../common/common';
import qs from 'qs';
import moment from 'moment';
class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            token: '',
            user: {},
        };
    }

    async componentWillMount() {
        try{
            let usuario = await common.getItem('usuario');
            console.log(usuario);
            let connected = await common.isConnected(async (connection) => {
                let usuario = await common.getItem('usuario');
                if(connection.type == 'none' && usuario.token.length > 1){
                    this.props.populateUser({...usuario, connected: false});
                }
                else{
                    this.props.populateUser({...usuario, connected: true});
                }
            });
            console.log('connected', connected);
            if(connected){
                let session = await this.checkToken(usuario.token);
                if(session.data.status == 200){
                    const aux = {...usuario, connected: true};
                    console.log(aux)
                    this.props.populateUser(aux);
                    let rondas = await
                    common.getItem('rondas')
                    .then((rondas)=>{
                        console.log(rondas);
                        this.props.populateRondas(rondas.rondas || rondas);
                        return common.getItem('puntos');
                    })
                    .then((puntos)=>{
                        this.props.populatePuntos(puntos);
                        this.props.sesion('main');
                    })
                    .catch((err)=>{
                        console.log(err);
                    });
                }
            }
            else{
                if(usuario.token.length > 1){
                    const [loadedRondas, loadedPuntos] = await Promise.all([common.getItem('rondas'), common.getItem('puntos')]);

                    console.log(loadedRondas);

                    this.props.populateUser({...usuario, connected: false});
                    this.props.populateRondas(loadedRondas.rondas || loadedRondas);
                    this.props.populatePuntos(loadedPuntos);
                    this.props.sesion('main');
                }

            }

        }
        catch(err){
                let encrypt = await this.requestEncrypt();
                let token = await this.requestToken(encrypt.data);
                console.log('the error',err);
                if(token.data.status == 200){
                    this.setState({token: token.data.auth_token});
                }
                else{
                    throw "Error en la app";
                }
        }
    }

    async checkToken(token){
        return axios({
            method: 'POST',
            url: route+'auth/check',
            headers: {
                "X-Cronos-Auth-Token": token
                }
            });
    }

    async requestEncrypt() {
        return axios({
            method: 'GET',
            url: route+'auth/encrypt'
          });
    }

    async requestToken(encrypt){
        return axios({
            method: 'POST',
            url: route+'auth/jwt_api',
            headers: {
                'X-Cronos-Access-Token': encrypt,
                }
            });
    }

    async requestLogin(usr,pwd, token){
        let headers = {
                "Client-Service": "web-client",
                "Auth-Key": "cronos-ak-v1",
                "X-Cronos-Auth-Token": token,
                "Content-Type": "application/x-www-form-urlencoded"
              }
        return axios.post(route+'auth/login',
            qs.stringify({
                    uid: usr,
                    pwd: pwd,
            }),
            {
              headers: headers
            });
    }

    async login(qr){
        qr = qr.split(';');
        if(qr.map((q)=> q.toLowerCase()).includes('celador')){
            let loginData;
            let savedRondas = await common.getItem('rondas');
            let time = await common.getItem('time');
            if(savedRondas !== null){
                if(time === null){
                    common.saveItem('time',moment());
                    console.log('here saving time');
                }
                else{
                    var then = moment(time);
                    var now = moment();
                    var aux = moment.duration(now.diff(then)).asMinutes();
                    console.log(aux);
                }
            }

            try{
                let encrypt = await this.requestEncrypt();
                let token = await this.requestToken(encrypt.data);
                loginData = await this.requestLogin(qr[0],qr[5],token.data.auth_token);
                if(loginData.data.status == 200){
                    console.log(loginData.data.result);
                    let rondas = loginData.data.result.rondas;
                    let tam = false;
                    let savedRondas = await common.getItem('rondas');
                    let time = await common.getItem('time');
                    if(savedRondas !== null){
                        tam = savedRondas.length == rondas.length;
                        if(time === null){
                            common.saveItem('time',moment());
                            console.log('here saving time');
                        }
                        else{
                            var then = moment(time);
                            var now = moment();
                            var aux = moment.duration(now.diff(then)).asDays();
                            tam = (aux >= 1)?false:tam;
                            common.saveItem('time',moment());
                            console.log(aux);
                        }
                    }
                    for(let i in rondas){
                        rondas[i] = {...rondas[i], puntos_hechos: 0, estado:0, enviada: (rondas[i].is_complete == "")?false:true};
                    }
                    let puntos = loginData.data.result.config_puntos;
                    for(let i in puntos){
                        puntos[i] = {...puntos[i], leido: false};
                    }
                    let usuario = {usuario: loginData.data.result, token: token.data.auth_token};
                    this.props.populateRondas(rondas.rondas || rondas);
                    this.props.populatePuntos(puntos);
                    this.props.populateUser(usuario);
                    console.log(usuario);
                    common.saveItem('bloques',[]);
										const bloques = (await this.requestBloques(usuario.usuario.data.id_edificio, usuario.token)).data.result;
										
										const bloquesAptos = await Promise.all(bloques.map(async (bloque) => {
											const aptos = await this.requestAptos(bloque.id, usuario.token);

											return { ...bloque, aptos: aptos.data.result }
										}));

                    common.saveItem('bloques',bloquesAptos);
                    common.saveItem('rondas',rondas);
                    common.saveItem('puntos',puntos);
                    common.saveItem('usuario',usuario);
                    this.props.sesion('main');
                }
                else{
                    Alert.alert('Ocurrio un error favor intente nuevamente');
                }
            }
            catch(err){
                Alert.alert('Revise su conexión a internet');
                console.log(err);
            }
        }
        else{
            Alert.alert('Solo puede iniciar bajo el rol de celador');
        }
    }

    async requestBloques(idEdificio, token){
        return axios({
            method: 'GET',
            url: route+'bloques/'+idEdificio,
            headers: {
                "Auth-Key": "cronos-ak-v1",
                "X-Cronos-Auth-Token": token,
                }
          });
		}
		
		async requestAptos(idBloque, token){
			return axios({
					method: 'GET',
					url: route+'apartamentos/'+idBloque,
					headers: {
							"Auth-Key": "cronos-ak-v1",
							"X-Cronos-Auth-Token": token,
							}
				});
	}

    actionRouter(action, payload={}){
        switch(action){
            case 'main':
               Actions.loginCamera({QR: true, getQR: this.login.bind(this), getPicture: {}});
            //    this.login('1118;celador;111;null;302;12345;cel;rec;1');
            break;
        }
    }

    render() {
        const {laterals, main, box, boxCenter} = styles;

        return (
            <View style={styles.container}>
                <View style={laterals}>
                </View>
                <View style={main}>
                    <View style={box}>
                    </View>
                    <View style={boxCenter}>
                        <ImgButton img={require('../images/login.png')}
                            onPress={() => this.actionRouter('main')}
                         ></ImgButton>
                    </View>
                    <View style={box}>
                    </View>
                </View>
                <View style={laterals}>
                </View>
            </View>
        );
      }
}

export default connect(null, actions)(Login);

const route = 'http://api.cronosseguridad.com/';
const styles = {
    container:{
        flex: 1,
        flexDirection: 'row'
    },
    laterals:{
        flex: 2,
        backgroundColor: 'white',
    },
    main: {
        flex: 2,
        flexDirection: 'column',
        backgroundColor: '#415d68',
        alignItems: 'center',
    },
    box:{
        flex: 1,
    },
    boxCenter:{
        flex: 1.6,

    }
};