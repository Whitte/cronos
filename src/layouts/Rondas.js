import React, {Component} from 'react';
import {View, FlatList} from 'react-native';
import BtnRonda  from '../components/BtnRonda';
import {BtnAction}  from '../components/BtnAction';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';


class Rondas extends Component{

    constructor(props) {
        super(props);
        this.props.setHeader('Rondas');
        this.state = {
            rondas: this.props.rondas
        }
    }

    render(){
        return(
            <View style={styles.view}>
                <View style={styles.main}>
                    <View style={styles.container}>
                            <FlatList contentContainerStyle={styles.list} 
                            numColumns={3}
                            keyExtractor={(item, index) => index}
                            data={this.state.rondas}
                            renderItem={({item}) => 
                            <BtnRonda style={styles.item} ronda={item}/>
                        }
                            />
                    </View>
                </View>
                <View style={styles.footer}>
                    <BtnAction img={require('../images/btn_volver.png')}  onPress={() => Actions.pop({refresh: {setHeader: this.props.setHeader}})}/>
                </View>
            </View>
            );
    }

}

const styles= {
    row: {
        flexDirection: 'row',
        paddingVertical: 6,
        justifyContent: 'space-between'
    },
    main: {
        flexDirection: 'row',
        flex: 5,
        paddingVertical: 3,
        justifyContent: 'space-between',
    },
    footer: {
        flexDirection: 'row',
        flex: 1,
        paddingHorizontal: 15,
        paddingVertical: 8,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    view:{
        flexDirection: 'column',
        justifyContent: 'space-around',
        flex:1,
    },
    container:{
        marginHorizontal: 115,
        justifyContent: 'space-around',
        flex:1,
    },
    list: {
        flexDirection: 'column',
    }
}

const mapStateToProps = (state) =>{
    let aux = {rondas: state.rondas.rondas}
    return aux;
}

export default connect(mapStateToProps)(Rondas);