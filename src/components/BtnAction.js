import React from 'react';
import { Image, Text, TouchableOpacity } from 'react-native';

const BtnAction = ({ img, txt, onPress }) => {
    let text = txt !== undefined;
    return (
        <TouchableOpacity onPress={onPress} style={styles(text).container}>
            {text && 
            <Text style={styles(text).text}>{txt}</Text>}
            {img !== undefined && 
            <Image 
                resizeMode={'contain'} 
                style={styles(text).image}
                source={img}
            />}
        </TouchableOpacity>
    );
};

export { BtnAction };

const styles = (text)=> ({
    container:{
        alignSelf: 'flex-start',
        height: (text)?40:60,
        alignItems: 'center',
        borderRadius: 16,
        borderWidth: 4,
        borderColor: '#97999c',
        backgroundColor: '#dedfe1',
        padding: 5,
        paddingHorizontal: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    image:{
        tintColor: '#3b464c',
        height: 25,
        marginLeft: (text)? 7:0
    },
    text:{
        color: '#3b464c',
        fontSize: 25,
        fontFamily: "cronos",
    }
});