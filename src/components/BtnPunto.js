import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, Alert } from 'react-native';
import {Actions} from 'react-native-router-flux';
import * as actions from '../actions';
import {connect} from 'react-redux';
import moment from 'moment';
import * as common from '../common/common';

class BtnPunto extends Component {

    constructor(props) {
        super(props);
        this.state = {
            punto: {...props.punto},
        }
    }

    async leerPunto(punto){
        let minutesGap = 1;
        let timePoint = await common.getItem('timePoint');
        let gap = false;
        if(timePoint !== null){
            let minutesElapsed = moment.duration(moment().diff(timePoint)).asMinutes();;
            gap = minutesElapsed > minutesGap;
            console.log(gap);
        }
        else{
            gap = true;
            console.log('timepoint false');
        }

        if(!this.state.punto.leido && gap){
            Actions.camera({QR: true, getQR: (qr) => {this.getQR(qr)}, getPicture: {}});
            // this.setState({punto:{...this.state.punto, leido: true, indice: false}});
            // this.props.leerPunto(this.state.punto);
            // this.props.actualizarPuntoHecho();
            // common.saveItem('timePoint', moment());
        }
    }

    getQR(qr){
        if(qr == this.state.punto.nombre_punto){
            this.setState({punto:{...this.state.punto, leido: true, indice: false}});
            this.props.leerPunto(this.state.punto);
            this.props.actualizarPuntoHecho();
            common.saveItem('timePoint', moment());
        }
        else{
            Alert.alert('Punto incorrecto');
        }
    }

    render() {
        return (
            <TouchableOpacity onPress={async () => this.leerPunto(this.state.punto)} style={[styles.container,{backgroundColor: (this.state.punto.leido)?'#4c6640':'#878787',}]}>
                <Image style={styles.image}
                       source={require('../images/qr_punto.png')}
                />
                <Text style={styles.text}>{this.state.punto.nombre_punto}</Text>
            </TouchableOpacity>
        );
    }
}

export default connect(null, actions)(BtnPunto);

const styles = {
    container:{
        alignItems: 'center',
        width: 160,
        height: 170,
        paddingVertical: 15,
        margin: 10,
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    image:{
        height: 120,
        width: 120,
    },
    text:{
        color: 'white',
        fontSize: 24,
        fontFamily: "cronos",
    }
};