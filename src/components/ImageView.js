import React from 'react';
import { Image, Text, View} from 'react-native';
import {BtnAction} from '../components/BtnAction';
import {Actions} from 'react-native-router-flux';

const ImageView = ({txt, img }) => {

    return (
        <View style={styles.view}>
            <View style={[styles.footer,{alignSelf: 'center'}]}>
                <Text style={styles.text}>{txt.toUpperCase()}</Text>
            </View>
            <View style={styles.main}>
                <Image style={styles.image} 
                source={{uri: img}}
                />
            </View>
            <View style={styles.footer}>
                <BtnAction img={require('../images/btn_volver.png')} onPress={() => Actions.pop()}/>
            </View>
        </View>
    );
};

export { ImageView };

const styles = {
    image:{
        width: '100%',
        height: '100%'
    },
    text:{
        color: '#415d68',
        fontSize: 25,
        fontFamily: "cronos",
    },
    main: {
        alignSelf: 'center',
        flexDirection: 'row',
        flex: 7,
        width: '60%',
        paddingVertical: 3,
        justifyContent: 'space-between',
    },
    footer: {
        flexDirection: 'row',
        flex: 1.5,
        paddingHorizontal: 15,
        paddingTop: 14,
        paddingBottom: 8,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    view:{
        flexDirection: 'column',
        flex:1,
    },
};