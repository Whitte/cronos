import React from 'react';
import { Image, Text, View} from 'react-native';

const IconText = ({ img, txt }) => {

    return (
        <View style={styles.container}>
            <Image style={styles.image}
                   source={require('../images/choose_icon.png')}
            />
            <Text style={styles.text}>[{txt}]</Text>
        </View>
    );
};

export { IconText };

const styles = {
    container:{
        alignItems: 'center',
        height: 80,
        flexDirection: 'column',
    },
    image:{
        height: 40,
        width: 40,
    },
    text:{
        color: '#3b464c',
        fontSize: 22,
        fontFamily: "cronos",
    }
};