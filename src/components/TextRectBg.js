import React from 'react';
import {Text, View} from 'react-native';

const TextRectBg = ({label, value, bgColor}) => {

    return (
        <View style={[styles.container, {backgroundColor: bgColor,}]}>
            <Text style={styles.text}>{label.toUpperCase()}{(label.length>0)?':':''} {value.toUpperCase()}</Text>
        </View>
    );
};

export { TextRectBg };

const styles = {
    container:{
        alignItems: 'center',
        height: 50,
        flexDirection: 'column',
        paddingHorizontal: 10,
        justifyContent: 'space-around',
        marginVertical: 5
    },
    text:{
        color: '#3b464c',
        fontSize: 22,
        fontFamily: "cronos",
        alignSelf: 'flex-start',
    }
};