import React, {Component} from 'react';
import {Text, TouchableOpacity } from 'react-native';

class BtnTextRect extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <TouchableOpacity style={styles.container}>
                <Text style={styles.text}>{txt='test'}</Text>
            </TouchableOpacity>
        );
    }
}

export default BtnTextRect;

const styles = {
    container:{
        alignItems: 'center',
        width: 100,
        height: 70,
        borderWidth: 4,
        borderColor: '#97999c',
        backgroundColor: '#dedfe1',
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    image:{
        tintColor: '#415d68',
        height: 90,
        width: 90,
    },
    text:{
        paddingTop: 15,
        color: '#415d68',
        fontSize: 24,
        fontFamily: "cronos",
    }
};