import React, {Component} from 'react';
import {
    View,
    Image,
    Text,
    TouchableOpacity,
    BackHandler
} from 'react-native';
import * as common from '../common/common';
import {Actions} from 'react-native-router-flux';


export default class Header extends Component{

    constructor(props) {
        super(props);
        const { setHeader } = this.props.setHeader || (() => {});
        BackHandler.addEventListener('hardwareBackPress', function() {
            Actions.pop({refresh: { setHeader }});
          });
    }

    sesion(){
        common.removeAll(()=>{
            this.props.sesion();
        });
    }

    render() {
        return (
            <View style={styles.containerHeader}>
                <TouchableOpacity>
                    <Image  style={styles.images}  source={iconsSource.emergency}/>
                </TouchableOpacity>

                <View style={styles.titleBox}>
                    <Text style={styles.titleText}>{this.props.title}</Text>
                </View>

                <TouchableOpacity onPress={() => this.sesion()}>
                    <Image style={styles.exit} source={iconsSource.logout}/>
                </TouchableOpacity>

            </View>
        );
    }
}


/** source of the header icons */
const iconsSource = {
    emergency: require('../images/boton_panico.png'),
    logout: require('../images/registrar_salida.png'),
};


/** styles */
const styles = {
    containerHeader: {
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: '#415d68',
        paddingRight: 15,
        flexDirection: 'row',
        height: 70,
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 50},
        shadowOpacity: 1,
        shadowRadius: 2,
    },
    images: {
        flex: 1,
        flexDirection: 'column',
        height: 70,
    },
    exit:{
        height: 40,
        width: 40,
        tintColor: 'white',
    },
    titleBox: {
        flex: 6,
        alignItems: 'center',
    },
    titleText:{
        fontSize: 35,
        color: 'white',
        fontFamily: "cronos",
    }

};
