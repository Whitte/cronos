import React, {Component} from 'react';
import { View, Text, TouchableOpacity, Alert } from 'react-native';
import {Actions} from 'react-native-router-flux';
import moment from 'moment';
import {connect} from 'react-redux';
import * as actions from '../actions';
import * as common from '../common/common';
import axios from 'axios';
import qs from 'qs';
const route = 'http://api.cronosseguridad.com/';


class BtnRonda extends Component {

    constructor(props){
        super(props);
        this.state = {
            ronda: props.ronda
        }
    }
    componentWillMount(){
        let estado = this.defineState(this.props.ronda, this.props.ronda.puntos_hechos);
        this.setState({estado});
    }

    async updateState(ronda,estado){
        if(!ronda.enviada){
            let auxRonda = this.getRondaJSON(ronda,estado);
            try{
                let connected = await common.isConnected();
                if(connected){
                    console.log('enviando ronda');
                    let res = await this.crearRonda(auxRonda);
                    if(res.data.status == 200){
                        console.log('here '+JSON.stringify(auxRonda));
                        this.props.actualizarRondaEnviada(ronda);
                        console.log('ronda enviada '+ronda.nombre_ronda+' '+ronda.is_complete);
                    }
                }
                else{
                    console.log('guardando rondas por enviar');
                    common.addItemTo('rondasPorEnviar',auxRonda);
                    this.props.actualizarRondaEnviada(ronda);
                }
                
            }
            catch(err){
                console.log(err);
            }
        }
    }

    defineState(ronda, puntos_hechos) {
        let format = 'hh:mm'
        let estate = 0;
        let time = moment(),
        beforeTime = moment(ronda.hora, format),
        afterTime = moment(ronda.hora, format).add(ronda.minutos,'minutes');

        // console.log(ronda.nombre_ronda+' '+ronda.is_complete+' - '+(time > beforeTime && puntos_hechos == this.props.total_puntos) +' -- '+ (ronda.is_complete === '1')+' + '+(time > beforeTime && puntos_hechos == this.props.total_puntos) || (ronda.is_complete === '1'));
        
        if (time.isBetween(beforeTime, afterTime) && puntos_hechos < this.props.total_puntos && ronda.is_complete === '') {
            estate = 2;
        } 
        else if((time > beforeTime && (puntos_hechos > 0 && puntos_hechos < this.props.total_puntos)) || ronda.is_complete == 2
    ) {
            this.updateState(ronda,2);
            estate = 4;
        }
        else if((time > beforeTime && puntos_hechos == this.props.total_puntos) || (ronda.is_complete === '1')
    ){
            this.updateState(ronda,1);
            estate = 3;
        }
        else if((time > beforeTime && puntos_hechos == 0) || ronda.is_complete === '0'
    ){
            this.updateState(ronda,0);
            estate = 5;
        }
        else{
            estate =1;
        }

        switch(estate){
            case 2:
                return {nombre: 'HACER RONDA', bgColor: '#f28d49', code: estate};
                break;
            case 3:
                return {nombre: 'COMPLETA', bgColor: '#415d68', code: estate};
                break;
            case 4:
                return {nombre: 'INCOMPLETA', bgColor: '#aa132d', code: estate};
                break;
            case 5:
                return {nombre: 'NO REALIZADA', bgColor: '#aa132d', code: estate};
                break;
            case 1:
            default:
                return {nombre: 'PENDIENTE', bgColor: '#878787', code: estate};
            break;
        }
    }

    actualizarPuntoHecho(){
        let puntosHechos = this.state.ronda.puntos_hechos+1;
        this.setState({ronda: {...this.state.ronda, puntos_hechos: puntosHechos}, estado: this.defineState(this.state.ronda,puntosHechos)},
            () => {
                this.props.actualizarPuntosHechos(puntosHechos);
                if(puntosHechos == this.props.total_puntos){
                    Actions.pop({refresh: {setHeader: this.props.setHeader}});
                    this.props.resetearPuntos();
                    // console.log(this.state.ronda);
                    Alert.alert('Ronda completada');
                }
            }
        );
    }

    onClick(action, payload={}){
        switch(action){
            case 'puntos':
            if(payload == 2){
                this.props.selectRonda(this.state.ronda);
                Actions.puntos({actualizarPuntoHecho: this.actualizarPuntoHecho.bind(this)})
            }
            else if(payload < 2){
                Alert.alert('No es tiempo de hacer la ronda');
            }
            break; 
            case 'no_ronda':
                Alert.alert('No es tiempo de hacer la ronda')
            break;
        }
    }

    getRondaJSON(ronda, estado){
        return {
            nombre: ronda.nombre_ronda,
            id_personal: this.props.usuario.data.id,
            is_complete: estado,
            fecha: moment().format('YYYY-MM-DD h:mm:ss'),
        }
    }

    async crearRonda(ronda){
        try{
            ronda = qs.stringify(ronda);
        }
        catch(err){
            console.log(err)
        }
        let headers = {
            "Auth-Key": "cronos-ak-v1",
            "X-Cronos-Auth-Token": this.props.token,
            "Content-Type": "application/x-www-form-urlencoded"
        };
        return axios.post(route+'rondas/create',
            ronda, 
            {
              headers: headers
            });
    }

    render() {
        return (
            <TouchableOpacity
                onPress={() => this.onClick('puntos',this.state.estado.code)} style={styles.container}>
                <View style={[styles.topBox, {backgroundColor: this.state.estado.bgColor}]}>
                    <Text style={styles.topText}>{this.state.ronda.nombre_ronda}</Text>
                </View>
                <View  style={[styles.Box, {backgroundColor: this.state.estado.bgColor}]}>
                    <View  style={styles.innerBox}/>
                    <View  style={[styles.innerBox, {backgroundColor: 'white', flex: 1}] }>
                        <Text style={styles.innerText}>{this.state.ronda.hora}</Text>
                    </View>
                    <View  style={styles.innerBox }>
                        <Text style={[styles.topText, {fontSize: 15, marginTop: 4}]}>{this.state.estado.nombre}</Text>
                    </View>

                </View>
            </TouchableOpacity>
        );
    }
}

const mapStateToProps = (state) =>{
    return {total_puntos: state.puntos.puntos.length
        , usuario: state.usuario.usuario, token: state.usuario.token};
}

export default connect(mapStateToProps, actions)(BtnRonda);

const styles = {
    container:{
        alignSelf: 'flex-start',
        flexDirection: 'column',
        width: 210,
        height: 120,
        margin: 10
    },
    topBox:{
        alignItems: 'center',
        backgroundColor: 'blue',
        width: 130,
        height: 30,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
    },
    Box:{
        backgroundColor: 'blue',
        width: 210,
        height: 90,
        borderTopRightRadius: 15,
        borderBottomRightRadius: 15,
        borderBottomLeftRadius: 15,
    },
    topText:{
        color: 'white',
        fontSize: 22,
        fontFamily: "cronos",
    },
    innerBox: {
        flex: 1,
        alignItems: 'center',
    },
    innerText:{
        alignSelf: 'center',
        fontSize: 22,
        fontFamily: "cronos",
    }
};