import React from 'react';
import { Image, Text, TouchableWithoutFeedback, View } from 'react-native';
import { CheckBox } from '../common'

const CkbImage = ({ img, txt, tint, check, onPress }) => {    
    return (
        <TouchableWithoutFeedback onPress={onPress} >
            <View style={styles.container}>
                <View style={styles.imgContainer}>
                    <Image  style={styles.image}
                            source={img}/>
                </View>
                <View style={styles.triangle}/>
                <View style={[styles.imgContainer,{backgroundColor: '#dedfe1'}]}>
                    <CheckBox check={check} onPress={onPress}/>
                </View>
                <View style={styles.txtContainer}>
                    <Text style={[styles.text]}>{txt.toUpperCase()}</Text>
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
};

export { CkbImage };

const styles = {
    container:{
        alignItems: 'center',
        height: 50,
        backgroundColor: '#dedfe1',
        margin: 5,
        flexDirection: 'row',
        alignItems: 'flex-start',
    },
    imgContainer:{
        backgroundColor: '#415d68',
        height: 50,
        flex: 1,
        justifyContent: 'space-around',
        alignItems: 'flex-end',
        paddingHorizontal: 15,
        marginRight: -2,
    },
    txtContainer:{
        backgroundColor: '#dedfe1',
        justifyContent: 'space-around',
        alignSelf: 'center',
        paddingHorizontal: 10,
        flex: 4,
    },
    image:{
        alignItems: 'center',
    },
    text:{
        color: '#3b464c',
        fontSize: 18,
        fontFamily: "cronos",
    },
    triangle:{
        borderWidth: 10,
        borderLeftColor: '#415d68',
        borderTopColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'transparent',
        height: 0,
        width: 0
    }
};