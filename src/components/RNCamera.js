import React, {Component} from 'react';

import {View,Text} from 'react-native';
import Camera from "react-native-camera";
import {Actions} from 'react-native-router-flux';
import { ImgButton } from '../common';

export default class RNCamera extends Component{
    constructor(props) {
        super(props);
        this.state = {
            data: this.props.data,
            camera: false,
            authtoken: this.props.authtoken,
            readed: false,
        };
    }

    returnQR(qr){
        this.props.getQR && this.props.getQR(qr);
        Actions.pop({refresh: {setHeader: this.props.setHeader}});
    }

    takePicture() {
        const options = {
        };
        this.camera.capture({metadata: options})
          .then((data) => {
              this.props.getPicture(data.path);
              Actions.pop({refresh: {setHeader: this.props.setHeader}});
            })
          .catch(err => console.error(err));
      }

    toggleCamera(){
        this.setState({camera: !this.state.camera});
    }

    
    render() {
        return (
            <View style={[styles.container,{backgroundColor:'red'}]}>
                <Camera
                    ref={(cam) => {
                        this.camera = cam;
                    }}
                    style={styles.preview}
                    aspect={Camera.constants.Aspect.fill}
                    onBarCodeRead={(res) => {if(!this.state.readed){this.returnQR(res.data)};this.setState({readed: true})}}
                    barCodeTypes={['org.iso.QRCode']}
                >
                <View>
                    {!this.props.QR && 
                    <ImgButton 
                    img={require('../images/buttons/cameraIcon.png')}
                    onPress={this.takePicture.bind(this)}></ImgButton>
                    // <Text style={styles.capture} onPress={this.takePicture.bind(this)}>CAPTURAR</Text>
                    }
                    <ImgButton 
                    img={require('../images/buttons/cancelIcon.png')}
                    onPress={() => {Actions.pop({refresh: {setHeader: this.props.setHeader}})}}></ImgButton>
                    {/* <Text style={styles.capture} onPress={() => {Actions.pop({refresh: {setHeader: this.props.setHeader}})}}>CANCELAR</Text> */}
                </View>
                </Camera>
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    preview: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        color: '#000',
        padding: 10,
        margin: 40,
        fontFamily: "cronos",
    }
};