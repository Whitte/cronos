import React from 'react';
import { Image, Text, TouchableOpacity } from 'react-native';

const BtnImageText = ({ img, txt, tint, onPress }) => {

    return (
        <TouchableOpacity style={styles.container}  onPress={onPress} >
            <Image resizeMode={'contain'} style={[styles.image,{tintColor: tint,}]}
                source={img}
            />
            <Text style={[styles.text]}>{txt.toUpperCase()}</Text>
        </TouchableOpacity>
    );
};

export { BtnImageText };

const styles = {
    container:{
        alignItems: 'center',
        width: 210,
        height: 180,
        borderRadius: 40,
        borderWidth: 4,
        borderColor: '#97999c',
        backgroundColor: '#dedfe1',
        paddingVertical: 22,
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    image:{
        height: 100,
    },
    text:{
        color: '#3b464c',
        fontSize: 25,
        fontFamily: "cronos",
    }
};