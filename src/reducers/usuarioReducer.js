const INITAL_STATE = {usuario: {connected: false}, token: null};
export default (state = INITAL_STATE, action) => {
    switch(action.type){
        case 'populate_user':
            let aux = {...state, usuario: action.payload.usuario, token: action.payload.token, connected: action.payload.connected};
            // console.log(aux);
            return aux;
        default:
            return state;
    }
}