const INITAL_STATE = {visitantes: []};
export default (state = INITAL_STATE, action) => {
    let visitantes= [];
    switch(action.type){
        case 'registrar_ingreso':
            visitantes = [...state.visitantes, action.payload];
            return {...state, visitantes: visitantes};
        case 'registrar_salida':
            visitantes = [...state.visitantes];
            visitantes.splice(action.payload,1);
            return {...state, visitantes: visitantes};
        default:
            return state;
    }
}