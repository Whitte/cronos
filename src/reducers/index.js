import {combineReducers} from 'redux';
import usuarioReducer from './usuarioReducer';
import puntosReducer from './puntosReducer';
import rondasReducer from './rondasReducer';
import visitanteReducer from './visitanteReducer';

export default combineReducers({
    usuario: usuarioReducer,
    rondas: rondasReducer,
    puntos: puntosReducer,
    visitantes: visitanteReducer,
});