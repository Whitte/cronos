import * as common from '../common/common';

const INITAL_STATE = {puntos: [], selectedPunto: null};
export default (state = INITAL_STATE, action) => {
    let puntos = [];
    switch(action.type){
        case 'populate_puntos':
             common.saveItem('puntos', action.payload);
            return {...state, puntos: action.payload};
        case 'leer_punto':
            puntos = [...state.puntos];
            let i = puntos.findIndex(index => index.nombre_punto == action.payload.nombre_punto);
            puntos[i] = action.payload;
            common.saveItem('puntos', puntos);

            return {...state, puntos: puntos};
        case 'resetear_puntos':
            puntos = [...state.puntos];
            for(let i in puntos){
                puntos[i] = {...puntos[i], leido: false};
            }
            common.saveItem('puntos', puntos);
            return {...state, puntos: puntos};
        default:
            return state;    
    }
}