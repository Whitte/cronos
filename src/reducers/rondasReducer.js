import * as common from '../common/common';

const INITAL_STATE = {rondas: [], selectedRonda: null};
export default (state = INITAL_STATE, action) => {
    let rondas= [];
    let index= 0;
    switch(action.type){
        case 'populate_rondas':
            let aux = {...state, rondas: action.payload};
            // console.log(action.payload);
            return aux;

        case 'select_ronda':            
            index = state.rondas.findIndex(index => index.nombre_ronda == action.payload.nombre_ronda);
            return {...state, selectedRonda: index};

        case 'actualizar_puntos_hechos':
            rondas = [...state.rondas];
            let ronda = rondas[state.selectedRonda];
            ronda = {...ronda, puntos_hechos: action.payload};
            rondas[state.selectedRonda] = ronda;
            rondas = {...state, rondas: rondas};
            common.saveItem('rondas', rondas);
            return rondas;

        case 'actualizar_ronda_enviada':
            index = state.rondas.findIndex(index => index.nombre_ronda == action.payload.nombre_ronda);
            rondas = [...state.rondas];
            rondas[index] = {...rondas[index], enviada: true};
            rondas = {...state, rondas: rondas};
            common.saveItem('rondas', rondas);
            return rondas;

        default:
            return state;
    }
}