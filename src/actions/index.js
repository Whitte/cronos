//Rondas actions
export const populateRondas = (rondas) => {
    return {
        type: 'populate_rondas',
        payload: rondas
    };
};

export const selectRonda = (rondaId) => {
    return {
        type: 'select_ronda',
        payload: rondaId
    };
};

export const actualizarPuntosHechos = (puntosHechos) => {
    return {
        type: 'actualizar_puntos_hechos',
        payload: puntosHechos
    };
};

export const actualizarRondaEnviada = (ronda) => {
    return {
        type: 'actualizar_ronda_enviada',
        payload: ronda
    };
};

//Puntos actions

export const populatePuntos = (puntos) => {
    return {
        type: 'populate_puntos',
        payload: puntos
    };
};

export const leerPunto = (punto) => {
    return {
        type: 'leer_punto',
        payload: punto
    };
};

export const resetearPuntos = () => {
    return {
        type: 'resetear_puntos',
        payload: {}
    };
};


//User actions

export const populateUser = (user) => {
    return {
        type: 'populate_user',
        payload: user
    };
};

//Visitantes actions
export const registrarVisitante = (visitante) =>{
    return {
        type: 'registrar_ingreso',
        payload: visitante
    };
};

export const registrarSalida = (index) =>{
    return {
        type: 'registrar_salida',
        payload: index
    };
};