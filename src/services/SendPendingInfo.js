import * as common from '../common/common';
import axios from 'axios';
import qs from 'qs';
import moment from 'moment';
import ImgToBase64 from 'react-native-image-base64';
const route = 'http://api.cronosseguridad.com/';

module.exports = async (taskData) => {
    let rondas = await common.getItem('rondasPorEnviar');
    let novedades = await common.getItem('novedades');
    console.log(rondas);
    console.log(novedades);
    let sendRondas = (rondas !== null)? rondas.length>0:false;
    let sendNovedades = (novedades !== null)? novedades.length>0:false;
    if(taskData.hasInternet && (sendRondas || sendNovedades)){
        let token;
        try{
            let encrypt = await requestEncrypt();
            token = await requestToken(encrypt.data);
            token = token.data.auth_token;
            console.log(token);
        }
        catch(err){
            console.log('hereee');
            console.log(err);
        }
        if(sendRondas && token.length>1){
            for(let ronda of rondas){
                try{
                    let res = await crearRonda(ronda, token);
                    if(res.data.status == 200){
                        console.log('ronda enviada '+JSON.stringify(ronda));
                        common.removeItemFrom('rondasPorEnviar',ronda);
                    }
                }
                catch(err){
                    console.log(err);
                }
            }
        }

        if(sendNovedades && token.length>1){
            for(let novedad of novedades){
                novedad = {
                    ...novedad,
                    imagen: await ImgToBase64.getBase64String(novedad.imagen)
                }
                try{
                    let res = await enviarNovedad(novedad, token);
                    if(res.data.status == 200){
                        console.log('nov enviada');
                        let mail = await enviarEmail('novedades',novedad, token);
                        if(mail.data.status == 200){
                            common.removeItemFrom('novedades',novedad);
                            console.log('email nov enviado');
                        }
                    }
                }
                catch(err){
                    console.log(err);
                }
            }
        }
    }
};

const  requestEncrypt = async() => {
    return axios({
        method: 'GET',
        url: route+'auth/encrypt'
      });
}

const  requestToken = async(encrypt) => {
    return axios({
        method: 'POST',
        url: route+'auth/jwt_api',
        headers: {
            'X-Cronos-Access-Token': encrypt,
            }
        });
}

const enviarNovedad = async (novedad,token) => {
    novedad = qs.stringify(novedad);
    let headers = {
        "Auth-Key": "cronos-ak-v1",
        "X-Cronos-Auth-Token": token,
        "Content-Type": "application/x-www-form-urlencoded"
    };
    return axios.post(route+'novedades/create',
        novedad, 
        {
          headers: headers
        });
}

const crearRonda = async (ronda, token) => {
    try{
        ronda = qs.stringify(ronda);
    }
    catch(err){
        console.log(err)
    }
    let headers = {
        "Auth-Key": "cronos-ak-v1",
        "X-Cronos-Auth-Token": token,
        "Content-Type": "application/x-www-form-urlencoded"
    };
    return axios.post(route+'rondas/create',
        ronda, 
        {
          headers: headers
        });
}

const enviarEmail = async function(asunto, data, token){
    try{
        data= {
            subject: asunto,
            date: moment().format('DD/MM/YYYY h:mm A'),
            id_edificio: data.id_edificio,
            id_personal: data.id_personal,
            extra: ''
        };
        data = qs.stringify(data);
        let headers = {
            "Auth-Key": "cronos-ak-v1",
            "X-Cronos-Auth-Token": token,
            "Content-Type": "application/x-www-form-urlencoded"
        };
        return axios.post(route+'email',
            data, 
            {
              headers: headers
            });
    }
    catch(err){
        console.log(err)
    }
}
