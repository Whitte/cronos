import PushNotification from 'react-native-push-notifications';
import { Alert } from 'react-native';

const localNotification = (id, title, message) => {
    PushNotification.localNotification({
        id: id,
        title: title,
        message: message,
        autoCancel: false,
        vibrate: true,
        vibration: 300,
        playSound: true,
        ongoing: false, // (optional) set whether this is an "ongoing" notification
        priority: "max", // (optional) set notification priority, default: high
        visibility: "public", // (optional) set notification visibility, default: private
        importance: "max",
        number: '10'
    });
};

const localNotificationSchedule = (id, title, message, date) =>{
    console.log('scheduling -id: '+id);
    PushNotification.localNotificationSchedule({
        id: id,
        title: title,
        message: message,
        date: date,
        autoCancel: false,
        vibrate: true,
        vibration: 300,
        playSound: true,
        ongoing: false, // (optional) set whether this is an "ongoing" notification
        priority: "max", // (optional) set notification priority, default: high
        visibility: "public", // (optional) set notification visibility, default: private
        importance: "max",
        number: '10'
    });
};

const cancelLocalNotification = (id) =>{
    console.log('canceling -id: '+id);
    PushNotification.cancelLocalNotifications({id: id});
};


const configure = () => {
    PushNotification.configure({

        onRegister: function(token) {
            console.log( 'TOKEN:', token );
        },

        // (required) Called when a remote or local notification is opened or received
        onNotification: function(notification) {
            console.log('NOTIFICATION:', notification);
            if(notification.foreground){
                // Alert.alert(notification.message);
                const { id, title, message} = notification;
                localNotification(id, title, message);
            }
        },

        permissions: {
            alert: true,
            badge: true,
            sound: true
        },

        popInitialNotification: true,
        requestPermissions: true,

    });
};

export {
    configure,
    localNotification,
    localNotificationSchedule,
    cancelLocalNotification
};


