import React, { Component } from 'react';
import Main from './layouts/Main';
import Login from './layouts/Auth';
import RNCamera from './components/RNCamera';
import {AppRegistry, View} from 'react-native';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import reducers from './reducers';
import {
  Scene, 
  Router, Actions
} from 'react-native-router-flux';
import { pushNotifications } from './services';
import  moment  from 'moment';
import Wrapper from './Wrapper';
import * as common from './common/common';

AppRegistry.registerHeadlessTask('SendPendingInfo', () => require('./services/SendPendingInfo'));

pushNotifications.configure();

export default class App extends Component {

    componentWillMount(){


        // let now = moment().format('MM DD YYYY');
        // now += ' 13:19';
        // let now2 = moment(now,'MM DD YYYY H:mm').toDate();

        // pushNotifications.localNotificationSchedule('Este es el titulo','Este es el programado 1', now2);

    }

    cerrarSesion(){
      Actions.auth();
    }

  render() {
    return (
      <Provider store={createStore(reducers)}>
        <Wrapper/>
      </Provider>
    );
  }
}

const styles = {
  container:{
      flex: 1,
  },
}