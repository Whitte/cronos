import {
    Scene, 
    Router, Actions} from 'react-native-router-flux';
import React, { Component } from 'react';
import Main from './layouts/Main';
import Login from './layouts/Auth';
import RNCamera from './components/RNCamera';
import {View} from 'react-native';

export default class Wrapper extends Component {
	constructor(props) {
        super(props);
        this.state = {
            logged: false,
        };
    }


    setSesion(action){
      switch(action){
        case 'main':
          this.setState({logged: true});
        break;
        case 'close':
          this.setState({logged: false});
        break;
      }
    }

  render() {
    return (
        <View style={styles.container}>
		  	{(!this.state.logged)?
			  <Router scenes={scenes} sesion={()=> this.setSesion('main')} hideNavBar />
		  :<Main sesion={()=> this.setSesion('close')}/>}
        </View>
    );
  }
}

const scenes = Actions.create(
  <Scene key='wrapper'>
    <Scene key='auth' component={Login} initial/>
    <Scene key='loginCamera' component={RNCamera}/>
  </Scene>
);

const styles = {
    container:{
        flex: 1,
    },
  }