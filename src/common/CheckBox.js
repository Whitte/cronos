import React from 'react';
import { Image, TouchableWithoutFeedback } from 'react-native';

const CheckBox = ({ check=false, onPress }) => {

    return (
        <TouchableWithoutFeedback style={styles.container}  onPress={onPress} >
            <Image resizeMode={'contain'}  style={[styles.image]}
                source={(check)?require('../images/buttons/check.png'):require('../images/buttons/uncheck.png')}
            />
        </TouchableWithoutFeedback>
    );
};

export { CheckBox };

const styles = {
    container:{
        alignItems: 'center',
        width: 80,
        height: 50,
        justifyContent: 'space-around',
    },
    image:{
        height: 60,        
    },
    text:{
        color: '#415d68',
        fontSize: 18,
        fontWeight: 'bold',
    }
};