import {AsyncStorage, NetInfo} from 'react-native';

export const saveItem = async (item, value) => {
    try {
        await AsyncStorage.setItem(item, JSON.stringify(value));
    } catch (error) {
        throw error;
    }
};

export const getItem= async (item)=>{
    let returned={};
    try{
        returned = await AsyncStorage.getItem(item);
        return JSON.parse(returned);
    }
    catch(err){
        throw err;
    }
};

export const addItemTo = async (item, value) => {
    let newItem=[];
    try{
        returned = await getItem(item);
        if(returned !== null){
            newItem = [...returned, value];
        }
        else{
            newItem = [...newItem,value];
        }
        await saveItem(item,newItem);
        console.log(newItem);
        return newItem;
    }
    catch(err){
        throw err;
    }
}

export const removeItemFrom = async (item, value) => {
    try{
        returned = await getItem(item);
        if(returned !== null && returned.length>0){
            let i = returned.findIndex(index => JSON.stringify(index) == JSON.stringify(value));
            returned.splice(i,1);
            await saveItem(item,returned);
            return returned;
        }
        return null;
    }
    catch(err){
        throw err;
    }
}

export const removeAll = async (cb) => {
    try{
        AsyncStorage.multiRemove(['usuario'], cb);
    }
    catch(err){
        throw err;
    }
}

export const isConnected = async (handler = () =>{}) => {
        if(handler !== null)NetInfo.addEventListener('connectionChange', handler);
        return await NetInfo.isConnected.fetch();
};