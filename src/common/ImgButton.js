import React from 'react';
import { Image, TouchableOpacity } from 'react-native';
import _ from 'lodash';

const ImgButton = ({ img, onPress}) => {
  const aux = _.debounce(onPress, 1000, { leading: true, trailing: false })

return (
  <TouchableOpacity  onPress={aux}>
    <Image
      source={img}
    />
  </TouchableOpacity>
  );
};

export { ImgButton };